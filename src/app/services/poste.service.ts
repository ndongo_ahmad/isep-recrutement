import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {ServeurService} from './server.service';
import {Injectable} from '@angular/core';
import {Poste} from '../models/poste';

@Injectable()
export class PosteService {
  baseURL: string;
  postes: Poste[];
  constructor(private http: HttpClient, private serveurRecrutementService: ServeurService) {
    this.baseURL = this.serveurRecrutementService.getBaseUrl();
  }
  getPostes(): Observable<Poste[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/postes', {headers: httpHeaders});
  }

  getPosteById(id: number): Observable<Poste> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/postes/' + id, {headers: httpHeaders});
  }
  createPoste(p: Poste): Observable<Poste> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/postes', p, {headers: httpHeaders});
  }
  editPoste(p: Poste): Observable<Poste> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/postes/' + p.id, p, {headers: httpHeaders});
  }
  deletePoste(p: Poste): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/postes/' + p.id, {headers: httpHeaders, responseType: 'text'});
  }
}
