import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {DossierCandidature} from '../models/dossier-candidature';
import {ServeurService} from './server.service';

@Injectable()
export class DossierCandidatureService {
  baseURL: string;
  dossierCandidatures: DossierCandidature[];
  constructor(private http: HttpClient,
              private serveurRecrutementService: ServeurService) {
    this.baseURL = this.serveurRecrutementService.getBaseUrl();
  }
  getDossierCandidatures(): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/dossierCandidature', {headers: httpHeaders});
  }
  getDossierCandidatureById(id: number): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/dossierCandidature/' + id, {headers: httpHeaders});
  }
  createDossierCandidature(c: DossierCandidature): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/dossierCandidature', c, {headers: httpHeaders});
  }
  editDossierCandidature(c: DossierCandidature): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/dossierCandidature/' + c.id, c, {headers: httpHeaders});
  }
  deleteDossierCandidature(c: DossierCandidature): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/dossierCandidature/' + c.id, {headers: httpHeaders, responseType: 'text'});
  }
}
