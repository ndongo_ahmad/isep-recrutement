import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ServeurService} from './server.service';
import {Injectable} from '@angular/core';
import {SessionRecrutement} from '../models/session-recrutement';

@Injectable()
export class SessionRecrutementService {
  baseURL: string;
  sessionRecrutements: SessionRecrutement[];
  constructor(private http: HttpClient, private serveurRecrutementService: ServeurService) {
    this.baseURL = this.serveurRecrutementService.getBaseUrl();
  }
  getSessionRecrutements(): Observable<SessionRecrutement[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/sessionRecrutement', {headers: httpHeaders});
  }

  getSessionRecrutementById(id: number): Observable<SessionRecrutement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/sessionRecrutement/' + id, {headers: httpHeaders});
  }
  createSessionRecrutement(d: SessionRecrutement): Observable<SessionRecrutement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/sessionRecrutement', d, {headers: httpHeaders});
  }
  editSessionRecrutement(d: SessionRecrutement): Observable<SessionRecrutement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/sessionRecrutement/' + d.id, d, {headers: httpHeaders});
  }
  deleteSessionRecrutement(d: SessionRecrutement): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/sessionRecrutement/' + d.id, {headers: httpHeaders, responseType: 'text'});
  }
}
