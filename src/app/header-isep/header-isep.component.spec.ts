import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderIsepComponent } from './header-isep.component';

describe('HeaderIsepComponent', () => {
  let component: HeaderIsepComponent;
  let fixture: ComponentFixture<HeaderIsepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderIsepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderIsepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
