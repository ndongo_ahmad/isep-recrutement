import { Component, OnInit } from '@angular/core';
import {Candidat} from '../models/candidat';
import {CandidatService} from '../services/candidat.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [CandidatService]
})
export class LoginComponent implements OnInit {
  candidat: Candidat;
  formLog: FormGroup;
  messError: string;
  mess: string;
  constructor(private candidatService: CandidatService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.candidat = new Candidat();
    this.formLog = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onValider() {
    if (this.formLog.invalid) {
      this.messError = 'Données invalides: Réessayez!';
    }
    this.candidatService.logCandidat(this.candidat).subscribe(
      () => {
        this.mess = 'Connecté avec succès!';
        setTimeout(
          () => this.mess = undefined,
          2000
        );
        /**LA SUITE ICI
         * ***
         */
      },
      error1 => {
        console.log(error1);
        this.messError = error1.error.message;
      }
    );
  }
}
