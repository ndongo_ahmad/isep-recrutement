import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {InscriptionComponent} from './inscription/inscription.component';
import {AccueilComponent} from './accueil/accueil.component';
import {MonDossierComponent} from './mon-dossier/mon-dossier.component';
import {HeaderIsepComponent} from './header-isep/header-isep.component';
import {PiecesJointesComponent} from './pieces-jointes/pieces-jointes.component';
import {ExperienceProComponent} from './experience-pro/experience-pro.component';
import {FormationAcaComponent} from './formation-aca/formation-aca.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'inscription',
    component: InscriptionComponent
  },
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path: 'header',
    component: HeaderIsepComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'accueil',
    component: AccueilComponent
  },
  {
    path: 'formation',
    component: FormationAcaComponent
  },
  {
    path: 'experience',
    component: ExperienceProComponent
  },
  {
    path: 'pieces-jointes',
    component: PiecesJointesComponent
  },
  {
    path: 'mon-dossier',
    component: MonDossierComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
