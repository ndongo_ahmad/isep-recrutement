import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationAcaComponent } from './formation-aca.component';

describe('FormationAcaComponent', () => {
  let component: FormationAcaComponent;
  let fixture: ComponentFixture<FormationAcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormationAcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationAcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
