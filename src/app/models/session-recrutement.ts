import {SessionPoste} from './session-poste';

export class SessionRecrutement {
  id: number;
  titre: string;
  description: string;
  dateDebut: Date;
  dateFin: Date;
  sessionPostes: SessionPoste[];
}
