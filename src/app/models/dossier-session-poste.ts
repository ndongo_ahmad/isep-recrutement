import {DossierSessionPostePk} from './dossier-session-poste-pk';
import {PieceJointe} from './piece-jointe';
import {Entretien} from './entretien';

export class DossierSessionPoste {
  pk: DossierSessionPostePk;
  piecesJointes: PieceJointe[];
  entretiens: Entretien[];
}
