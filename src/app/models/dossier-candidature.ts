import {Candidat} from './candidat';
import {ExperiencePro} from './experience-pro';
import {Diplome} from './diplome';
import {Commentaire} from './commentaire';

export class DossierCandidature {
  id: number;
  etat: string;
  candidat: Candidat;
  experiencePros: ExperiencePro[];
  diplomes: Diplome[];
  commentaires: Commentaire[];
}
