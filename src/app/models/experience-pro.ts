import {DossierCandidature} from './dossier-candidature';

export class ExperiencePro {
  id: number;
  posteOccupe: string;
  dateDebut: Date;
  dateFin: Date;
  entreprise: string;
  description: string;
  dossierCandidature: DossierCandidature;
}
