import {SessionPoste} from './session-poste';

export class Poste {
  id: number;
  nom: string;
  sessionPostes: SessionPoste[];
}
