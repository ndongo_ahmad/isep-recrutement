import {DossierCandidature} from './dossier-candidature';
import {Poste} from './poste';

export class DossierPertinentPk {
  dossierCandidature: DossierCandidature;
  poste: Poste;
}
