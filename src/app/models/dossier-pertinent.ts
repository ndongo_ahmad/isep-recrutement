import {DossierPertinentPk} from './dossier-pertinent-pk';

export class DossierPertinent {
  id: number;
  dateCreation: Date;
  observation: string;
  pk: DossierPertinentPk;
}
