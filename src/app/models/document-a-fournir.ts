import {SessionPoste} from './session-poste';

export class DocumentAFournir {
  id: number;
  nom: string;
  description: string;
  sessionPoste: SessionPoste;
}
