import {DossierCandidature} from './dossier-candidature';

export class Commentaire {
  id: number;
  corps: string;
  dossierCandidature: DossierCandidature;
}
