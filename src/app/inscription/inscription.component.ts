import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Candidat} from '../models/candidat';
import {CandidatService} from '../services/candidat.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {error} from 'util';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css'],
  providers: [CandidatService]
})
export class InscriptionComponent implements OnInit {
  candidat: Candidat;
  formIns: FormGroup;
  @Output() back: EventEmitter<Candidat> = new EventEmitter();
  message: string;
  submitting: boolean;
  constructor(private candidatService: CandidatService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.candidat = new Candidat();
    this.formIns = this.formBuilder.group(
      {
        prenom: ['', Validators.required],
        nom: ['', Validators.required],
        email: ['', Validators.required],
        username: ['', Validators.required],
        password: ['', Validators.required],
        confPassword: ['', Validators.required],
      }
    );
  }
  get f () {
    return this.formIns.controls;
  }
  onSoumettre() {
    if (this.f.password === this.f.confPassword) {
      this.submitting = true;
      if (this.candidat.id === undefined) {
        this.candidatService.createCandidat(this.candidat).subscribe(
          c => {
            this.back.emit(c);
            this.submitting = false;
            this.message = 'Inscription reussie!';
            setTimeout(
              () => this.message = undefined,
              2000
            );
            /**LA SUITE PAR ICI**/
          },
          err => {
            this.message = err.error.message;
          }
        );
      } else {
        this.candidatService.editCandidat(this.candidat).subscribe(
          c => {
            this.back.emit(c);
            this.submitting = false;
            this.message = 'Inscription reussie!';
            setTimeout(
              () => this.message = undefined,
              2000
            );
            /**LA SUITE PAR ICI**/
          },
          err => {
            this.message = err.error.message;
          }
        );
      }
    } else {
      this.message = 'Veuillez confirmer votre mot de passe';
    }
  }
}
