import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ServeurService} from './services/server.service';
import {HeaderIsepComponent} from './header-isep/header-isep.component';
import {AccueilComponent} from './accueil/accueil.component';
import { MonDossierComponent } from './mon-dossier/mon-dossier.component';
import { FormationAcaComponent } from './formation-aca/formation-aca.component';
import { ExperienceProComponent } from './experience-pro/experience-pro.component';
import { PiecesJointesComponent } from './pieces-jointes/pieces-jointes.component';
import { ProfileComponent } from './profile/profile.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InscriptionComponent,
    AccueilComponent,
    HeaderIsepComponent,
    MonDossierComponent,
    FormationAcaComponent,
    ExperienceProComponent,
    PiecesJointesComponent,
    ProfileComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ServeurService],
  bootstrap: [AppComponent]
})
export class AppModule { }
